@extends('part.layout')

@section('content')

<!--BEGIN CONTENT-->
<div id="content">
    <div class="content">
        <div class="breadcrumbs">
            <a href="/">Главная</a>
            <img src="{{asset('images/marker_2.gif')}}" alt=""/>
            <span>Каталог</span>
        </div>
        <div class="main_wrapper">
<h1><strong>Каталог</strong></h1>
    <div class="catalog_sidebar">
    </div>
            <div class="main_catalog">
                <ul class="catalog_table">
                    @foreach($auto as $autoVal)
                        @foreach($autoVal->performance as $autoValPerf)
                            <li>
                                <a href="#" class="thumb"><img src="images/placeholders/165x119.gif" alt=""/></a>
                                <div class="catalog_desc">
                                    {{--<div class="location">Location: Berlin, Germany</div>--}}
                                    <div class="title_box">
                                        <h4>{{$autoVal->label}}</h4>
                                        <div class="price">{{$autoValPerf->price}}</div>
                                    </div>
                                    <div class="clear"></div>
                                    <div class="grey_area">
                                        {{--<span>Registration 2002</span>--}}
                                        <span>{{$autoValPerf->engine}}</span>
                                        <span>{{$autoValPerf->transmission}}</span>
                                        <span>{{$autoVal->class}}</span>
                                        <span>{{$autoValPerf->color}}</span>
                                        @foreach($autoValPerf->helper as $helper)
                                            <span>{{$helper->name}}</span>
                                        @endforeach
                                    </div>
                                    {{--<a href="#" class="more markered">View details</a>--}}
                                </div>
                            </li>
                        @endforeach
                    @endforeach
                </ul>
                <div class="bottom_catalog_box">
                    {{$auto->links('part.paginate')}}
                </div>
                <div class="clear"></div>
            </div>
</div>
<div class="clear"></div>
</div>
</div>
<!--EOF CONTENT-->
<script type="text/javascript" src="{{asset('js/catalog_pagination.js')}}"></script>

    @stop