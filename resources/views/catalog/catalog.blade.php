@extends('part.layout')

@section('content')

    <!--BEGIN CONTENT-->
    <div id="content">
        <div class="content">
            <div class="breadcrumbs">
                <a href="/">Главная</a>
                <img src="{{asset('images/marker_2.gif')}}" alt=""/>
                <span>Каталог</span>
            </div>
            <div class="main_wrapper">
                {{--<div class="cars_categories">--}}
                    {{--<span>Все машины</span>--}}
                    {{--<a href="#"><span>Новые</span></a>--}}
                    {{--<a href="#"><span>Поддержанные</span></a>--}}
                {{--</div>--}}
                <h1><strong>Каталог</strong></h1>
                <div class="catalog_sidebar">
                    {{--<div class="search_auto">--}}
                        {{--<h3><strong>Поиск</strong> автомоблей</h3>--}}
                        {{--<div class="categories">--}}
                            {{--<input type="radio" id="search_radio_1" checked="checked" name="catalog"/>--}}
                            {{--<label for="search_radio_1" class="search_radio_1"></label>--}}
                            {{--<input type="radio" id="search_radio_2" name="catalog"/>--}}
                            {{--<label for="search_radio_2" class="search_radio_2"></label>--}}
                            {{--<input type="radio" id="search_radio_3" name="catalog"/>--}}
                            {{--<label for="search_radio_3" class="search_radio_3"></label>--}}
                            {{--<input type="radio" id="search_radio_4" name="catalog"/>--}}
                            {{--<label for="search_radio_4" class="search_radio_4"></label>--}}
                        {{--</div>--}}
                        {{--<div class="clear"></div>--}}
                        {{--<label><strong>Производитель:</strong></label>--}}
                        {{--<div class="select_box_1">--}}
                            {{--<select class="select_3">--}}
                                {{--<option value="0">Любой</option>--}}
                                {{--<option value="1">Audi</option>--}}
                                {{--<option value="2">Mercedes-Benz</option>--}}
                                {{--<option value="3">BMW</option>--}}
                                {{--<option value="4">Lexus</option>--}}
                                {{--<option value="5">Lincoln</option>--}}
                                {{--<option value="6">Ford</option>--}}
                                {{--<option value="7">Fiat</option>--}}
                                {{--<option value="8">Dodge</option>--}}
                            {{--</select>--}}
                        {{--</div>--}}
                        {{--<label><strong>Модель:</strong></label>--}}
                        {{--<div class="select_box_1">--}}
                            {{--<select class="select_3">--}}
                                {{--<option value="0">Любая</option>--}}
                                {{--<option value="1">R8</option>--}}
                                {{--<option value="2">S500</option>--}}
                                {{--<option value="3">540i</option>--}}
                                {{--<option value="4">RX300</option>--}}
                                {{--<option value="5">Navigator</option>--}}
                                {{--<option value="6">Taurus</option>--}}
                                {{--<option value="7">Doblo</option>--}}
                                {{--<option value="8">Viper</option>--}}
                            {{--</select>--}}
                        {{--</div>--}}
                        {{--<label><strong>Год:</strong></label>--}}
                        {{--<div class="select_box_2">--}}
                            {{--<select class="select_4">--}}
                                {{--<option value="0">С</option>--}}
                                {{--<option value="1">2013</option>--}}
                                {{--<option value="2">2012</option>--}}
                                {{--<option value="3">2011</option>--}}
                                {{--<option value="4">2010</option>--}}
                                {{--<option value="5">2009</option>--}}
                                {{--<option value="6">2008</option>--}}
                                {{--<option value="7">2007</option>--}}
                                {{--<option value="8">2006</option>--}}
                            {{--</select>--}}
                            {{--<select class="select_4">--}}
                                {{--<option value="0">По</option>--}}
                                {{--<option value="1">2013</option>--}}
                                {{--<option value="2">2012</option>--}}
                                {{--<option value="3">2011</option>--}}
                                {{--<option value="4">2010</option>--}}
                                {{--<option value="5">2009</option>--}}
                                {{--<option value="6">2008</option>--}}
                                {{--<option value="7">2007</option>--}}
                                {{--<option value="8">2006</option>--}}
                            {{--</select>--}}
                            {{--<div class="clear"></div>--}}
                        {{--</div>--}}
                        {{--<label><strong>Цена:</strong></label>--}}
                        {{--<div class="select_box_2">--}}
                            {{--<select class="select_4">--}}
                                {{--<option value="0">С</option>--}}
                                {{--<option value="1">1000</option>--}}
                                {{--<option value="2">2000</option>--}}
                                {{--<option value="3">3000</option>--}}
                                {{--<option value="4">4000</option>--}}
                                {{--<option value="5">5000</option>--}}
                                {{--<option value="6">6000</option>--}}
                                {{--<option value="7">7000</option>--}}
                                {{--<option value="8">8000</option>--}}
                            {{--</select>--}}
                            {{--<select class="select_4">--}}
                                {{--<option value="0">По</option>--}}
                                {{--<option value="1">1000</option>--}}
                                {{--<option value="2">2000</option>--}}
                                {{--<option value="3">3000</option>--}}
                                {{--<option value="4">4000</option>--}}
                                {{--<option value="5">5000</option>--}}
                                {{--<option value="6">6000</option>--}}
                                {{--<option value="7">7000</option>--}}
                                {{--<option value="8">8000</option>--}}
                            {{--</select>--}}
                            {{--<div class="clear"></div>--}}
                        {{--</div>--}}
                        {{--<label><strong>Mileage:</strong></label>--}}
                        {{--<div class="select_box_2">--}}
                            {{--<select class="select_4">--}}
                                {{--<option value="0">From</option>--}}
                                {{--<option value="1">1000</option>--}}
                                {{--<option value="2">2000</option>--}}
                                {{--<option value="3">3000</option>--}}
                                {{--<option value="4">4000</option>--}}
                                {{--<option value="5">5000</option>--}}
                                {{--<option value="6">6000</option>--}}
                                {{--<option value="7">7000</option>--}}
                                {{--<option value="8">8000</option>--}}
                            {{--</select>--}}
                            {{--<select class="select_4">--}}
                                {{--<option value="0">To</option>--}}
                                {{--<option value="1">1000</option>--}}
                                {{--<option value="2">2000</option>--}}
                                {{--<option value="3">3000</option>--}}
                                {{--<option value="4">4000</option>--}}
                                {{--<option value="5">5000</option>--}}
                                {{--<option value="6">6000</option>--}}
                                {{--<option value="7">7000</option>--}}
                                {{--<option value="8">8000</option>--}}
                            {{--</select>--}}
                            {{--<div class="clear"></div>--}}
                        {{--</div>--}}
                        {{--<div class="chb_wrapper">--}}
                            {{--<input type="checkbox"/>--}}
                            {{--<label class="check_label">Only new cars</label>--}}
                        {{--</div>--}}
                        {{--<input type="submit" value="Поиск" class="btn_search"/>--}}
                        {{--<div class="clear"></div>--}}
                    {{--</div>--}}
                    {{--<div class="calculator">--}}
                        {{--<h3><strong>Loan</strong> calculator</h3>--}}
                        {{--<label><strong>Loan Amount:</strong></label>--}}
                        {{--<div class="select_box_1">--}}
                            {{--<select class="select_3">--}}
                                {{--<option value="0">0.00 Euro</option>--}}
                                {{--<option value="1">10.00 Euro</option>--}}
                                {{--<option value="2">100.00 Euro</option>--}}
                                {{--<option value="3">1000.00 Euro</option>--}}
                                {{--<option value="4">10000.00 Euro</option>--}}
                                {{--<option value="5">100000.00 Euro</option>--}}
                                {{--<option value="6">1000000.00 Euro</option>--}}
                                {{--<option value="7">10000000.00 Euro</option>--}}
                                {{--<option value="8">100000000.00 Euro</option>--}}
                            {{--</select>--}}
                        {{--</div>--}}
                        {{--<label><strong>Down Payment:</strong></label>--}}
                        {{--<div class="select_box_1">--}}
                            {{--<select class="select_3">--}}
                                {{--<option value="0">0.00 Euro</option>--}}
                                {{--<option value="1">10.00 Euro</option>--}}
                                {{--<option value="2">100.00 Euro</option>--}}
                                {{--<option value="3">1000.00 Euro</option>--}}
                                {{--<option value="4">10000.00 Euro</option>--}}
                                {{--<option value="5">100000.00 Euro</option>--}}
                                {{--<option value="6">1000000.00 Euro</option>--}}
                                {{--<option value="7">10000000.00 Euro</option>--}}
                                {{--<option value="8">100000000.00 Euro</option>--}}
                            {{--</select>--}}
                        {{--</div>--}}
                        {{--<label><strong>Annual Rate:</strong></label>--}}
                        {{--<div class="select_box_1">--}}
                            {{--<select class="select_3">--}}
                                {{--<option value="0">0.00 %</option>--}}
                                {{--<option value="1">10.00 %</option>--}}
                                {{--<option value="2">20.00 %</option>--}}
                                {{--<option value="3">30.00 %</option>--}}
                                {{--<option value="4">40.00 %</option>--}}
                                {{--<option value="5">50.00 %</option>--}}
                            {{--</select>--}}
                        {{--</div>--}}
                        {{--<label><strong>Loan Period:</strong></label>--}}
                        {{--<div class="select_box_1">--}}
                            {{--<select class="select_3">--}}
                                {{--<option value="0">3 Years</option>--}}
                                {{--<option value="1">4 Years</option>--}}
                                {{--<option value="2">5 Years</option>--}}
                                {{--<option value="3">6 Years</option>--}}
                                {{--<option value="4">7 Years</option>--}}
                            {{--</select>--}}
                        {{--</div>--}}
                        {{--<input type="submit" value="calculate" class="btn_calc"/>--}}
                        {{--<div class="clear"></div>--}}
                    {{--</div>--}}
                </div>
                <div class="main_catalog">
                    {{--<div class="top_catalog_box">--}}
                        {{--<div class="switch">--}}
                            {{--<span class="table_view"></span>--}}
                            {{--<a href="#" class="list_view"></a>--}}
                        {{--</div>--}}
                        {{--<div class="sorting drop_list">--}}
                            {{--<strong>Sort by: </strong>--}}
                            {{--<div class="selected">--}}
                                {{--<span><a href="#">Date</a></span>--}}
                                {{--<ul>--}}
                                    {{--<li><a href="#">Date</a></li>--}}
                                    {{--<li><a href="#">Price</a></li>--}}
                                    {{--<li><a href="#">Name</a></li>--}}
                                    {{--<li><a href="#">Manufacturer</a></li>--}}
                                {{--</ul>--}}
                            {{--</div>--}}
                            {{--<div class="clear"></div>--}}
                        {{--</div>--}}
                        {{--<div class="view_on_page drop_list">--}}
                            {{--<strong>View on page:</strong>--}}
                            {{--<div class="selected">--}}
                                {{--<span><a href="#">10</a></span>--}}
                                {{--<ul>--}}
                                    {{--<li><a href="#">10</a></li>--}}
                                    {{--<li><a href="#">20</a></li>--}}
                                    {{--<li><a href="#">50</a></li>--}}
                                    {{--<li><a href="#">100</a></li>--}}
                                {{--</ul>--}}
                            {{--</div>--}}
                            {{--<div class="clear"></div>--}}
                        {{--</div>--}}
                        {{--<div class="pagination">--}}
                            {{--<ul>--}}
                                {{--<li class="active"><a href="#">1</a></li>--}}
                                {{--<li><a href="#">2</a></li>--}}
                                {{--<li><a href="#">3</a></li>--}}
                                {{--<li class="space">...</li>--}}
                                {{--<li><a href="#">8</a></li>--}}
                                {{--<li class="next"><a href="#"><img src="images/page_next.gif" alt=""/></a></li>--}}
                            {{--</ul>--}}
                        {{--</div>--}}
                        {{--<div class="clear"></div>--}}
                    {{--</div>--}}
                    <ul class="catalog_table">

                        @foreach($auto as $autoVal)
                        <li>
                            <a href="#" class="thumb"><img src="images/placeholders/165x119.gif" alt=""/></a>
                            <div class="catalog_desc">
                                {{--<div class="location">Location: Berlin, Germany</div>--}}
                                <div class="title_box">
                                    <h4>{{$autoVal['label']}}</h4>
                                    <div class="price">{{$autoVal['price']}}</div>
                                </div>
                                <div class="clear"></div>
                                <div class="grey_area">
                                    <span>Registration 2002</span>
                                    <span>{{$autoVal['engine']}}</span>
                                    <span>{{$autoVal['transmission']}}</span>
                                    <span>{{$autoVal['class']}}</span>
                                    <span>{{$autoVal['color']}}</span>
                                    @foreach($autoVal['helpers'] as $helper)
                                    <span>{{$helper}}</span>
                                    @endforeach
                                </div>
                                {{--<a href="#" class="more markered">View details</a>--}}
                            </div>
                        </li>
                        @endforeach
                    </ul>
                    <div class="bottom_catalog_box">
                        <div class="pagination">
                            <ul>
                                {{$auto->links()}}
                                <li class="active"><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li class="space">...</li>
                                <li><a href="#">8</a></li>
                                <li class="next"><a href="#"><img src="images/page_next.gif" alt=""/></a></li>
                            </ul>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
    <!--EOF CONTENT-->

    @stop