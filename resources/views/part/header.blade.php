<!--BEGIN HEADER-->
<header>
<div id="header">
    <div class="top_info">
        <div class="logo">
            <a href="#">Автосалон<span>"Центральный"</span></a>
        </div>
        {{--<div class="header_contacts">--}}
            {{--<div class="phone">+1 (800) 455-55-95</div>--}}
            {{--<div>WinterJuice, LLC, 1875 South Grant Street, Suite 680, San Mateo, CA 94402</div>--}}
        {{--</div>--}}
        {{--<div class="socials">--}}
            {{--<a href="#"><img src="{{asset('images/fb_icon.png')}}" alt=""></a>--}}
            {{--<a href="#"><img src="images/twitter_icon.png" alt=""></a>--}}
            {{--<a href="#"><img src="images/in_icon.png" alt=""></a>--}}
        {{--</div>--}}
    </div>
    <div class="bg_navigation">
        <div class="navigation_wrapper">
            <div id="navigation">
                <span>Главная</span>
                <ul>
                    <li><a href="/">Главная</a></li>
                    <li><a href="{{route('catalog')}}">Каталог</a></li>
                    {{--<li><a href="#">News</a></li>--}}
                    {{--<li><a href="#">For Salers</a></li>--}}
                    <li><a href="{{route('about')}}">О нас</a></li>
                    <li><a href="{{route('contacts')}}">Контакты</a></li>
                </ul>
            </div>
            <div id="search_form">
                <form method="get" action="#">
                    <input type="text" onblur="if(this.value=='') this.value='Поиск...';" onfocus="if(this.value=='Поиск...') this.value='';" value="Поиск..." class="txb_search"/>
                    <input type="submit" value="Поиск" class="btn_search"/>
                </form>
            </div>
        </div>
    </div>
</div>
</header>
<!--EOF HEADER-->

