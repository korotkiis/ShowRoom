<!DOCTYPE html>
<html lang="ru">
<head>
    <meta name="description" content="{{ $description }}">
    <title>{{ $title }}</title>
    <script type="text/javascript" src="{{asset('js/jquery-1.8.3.min.js')}}"></script>

    @include('part.styles')

</head>

<body>

@include('part.header')

@yield('content')

@include('chat.chat')
@include('part.footer')

@include('part.scripts')
</body>
</html>