<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index')->with('description', 'Главная страница')
        ->with('title', 'Автосалон "Центральный"');
});



Route::get('about', 'AboutController@index')->name('about');

Route::get('contact', 'ContactController@index')->name('contacts');

Route::get('catalog', 'CatalogController@getCarPagination')->name('catalog');

Route::get('catalog_scroll', 'CatalogController@getCarScroll');

Route::post('catalog_scroll', 'CatalogController@getCarAjax');

Route::post('stat', 'CatalogController@stat');

