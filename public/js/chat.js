$(document).ready(function(){
    //create a new WebSocket object.
    var wsUri = "ws://localhost:9000/";
    websocket = new WebSocket(wsUri);

    websocket.onopen = function(ev) { // connection is open
        $('#message_box').append("<div class=\"system_msg\">Подключено!</div>"); //notify user
    }

    $('#send-btn').click(function(){ //use clicks message send button
        var mymessage = $('#message').val(); //get message text
        var myname = $('#name').val(); //get user name

        if(myname == ""){ //empty name?
            alert("Введите ваше имя!");
            return;
        }
        if(mymessage == ""){ //emtpy message?
            alert("Введите сообщение!");
            return;
        }

        //prepare json data
        var msg = {
            message: mymessage,
            name: myname,
        };
        //convert and send data to server
        websocket.send(JSON.stringify(msg));
    });

    //#### Message received from server?
    websocket.onmessage = function(ev) {
        var msg = JSON.parse(ev.data); //PHP sends Json data
        var type = msg.type; //message type
        var umsg = msg.message; //message text
        var uname = msg.name; //user name
        // var ucolor = msg.color; //color

        if(type == 'usermsg')
        {
            $('#message_box').append("<div><span class=\"user_name\" style=\"color: black"+"\">"+uname+"</span> : <span class=\"user_message\">"+umsg+"</span></div>");
        }
        if(type == 'system')
        {
            $('#message_box').append("<div class=\"system_msg\">"+umsg+"</div>");
        }

        $('#message').val(''); //reset text
    };

    websocket.onerror   = function(ev){$('#message_box').append("<div class=\"system_error\">Ошибка - "+ev.data+"</div>");};
    websocket.onclose   = function(ev){$('#message_box').append("<div class=\"system_msg\">Подключение закрыто</div>");};
});

$('.js-click-modal').click(function(){
    $('.container').addClass('modal-open');
});

$('.js-close-modal').click(function(){
    $('.container').removeClass('modal-open');
});


var StickyElement = function(node){
    var doc = $(document),
        fixed = false,
        anchor = node.find('.sticky-anchor'),
        content = node.find('.sticky-content');

    var onScroll = function(e){
        var docTop = doc.scrollTop(),
            anchorTop = anchor.offset().top;

        // console.log('scroll', docTop, anchorTop);

        if(docTop > anchorTop){
            if(!fixed){
                anchor.height(content.outerHeight());
                content.addClass('fixed');
                fixed = true;
            }
        }  else   {
            if(fixed){
                anchor.height(0);
                content.removeClass('fixed');
                fixed = false;
            }
        }
    };

    $(window).on('scroll', onScroll);
};
var demo = new StickyElement($('#sticky'));


