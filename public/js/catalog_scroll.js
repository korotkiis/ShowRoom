$(document).ready(function(){

    var inProgress = false; // статус процесса загрузки
    var startFrom = 2; // позиция с которой начинается вывод данных

    $(window).scroll(function() {


        // высота окна + высота прокрутки больше или равны высоте всего документа
        if ($(window).scrollTop() + $(window).height() >= $(document).height() - 200 && !inProgress) {
            $.ajax({
                url: 'catalog_scroll', // путь к ajax-обработчику
                type: 'POST',
                data: {
                    "start" : startFrom
                },
                beforeSend: function() {
                    inProgress = true;
                },
                success: function (data) {
                        data = jQuery.parseJSON(data); // данные в json

                    if (data.length > 0){
                            // добавляем записи в блок в виде html
                            $.each(data, function(index, car){
                                $.each(car.performance, function (index, carPerf) {
                                    $(".catalog_table").append(
                                        "<li> <a href='#' class='thumb'>" +
                                        "<img src='images/placeholders/165x119.gif' alt=''/></a> " +
                                        "<div class='catalog_desc'> " +
                                        "<div class='title_box'>" +
                                        "<h4>" + car.label+ "</h4>" +
                                        "<div class='price'>" + carPerf.price+ "</div> " +
                                        "</div> " +
                                        "<div class='clear'></div> " +
                                        "<div class='grey_area'> " +
                                        "<span>"+carPerf.engine+"</span> " +
                                        "<span>"+carPerf.transmission+"</span> " +
                                        "<span>"+car.class+"</span> " +
                                        "<span>"+carPerf.color+"</span>"+
                                        carPerf.helper.map(function (helper){
                                            return "<span>"+helper.name+"</span> ";
                                        }).join('')
                                        +
                                        "</div> " +
                                        "</div> " +
                                        "</li>"
                                    );
                                })

                            });
                            inProgress = false;
                            startFrom += 2;
                        }
                },
                error: function(data){
                    console.log(data)
                }
            });
        }
    });
});