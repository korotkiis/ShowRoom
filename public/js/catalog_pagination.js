$(window).on('hashchange', function () {
    if (window.location.hash) {

        var page = window.location.hash.replace('#', '');

        if (page == Number.NaN || page <= 0) {

            return false;

        } else {

            getData(page);

        }
    }
});

function getData(page) {

    $.ajax(
        {
            url: '?page=' + page,
            type: "get",
            datatype: "html"
        }).done(function (data) {
        $("#content").empty().html(data);
        location.hash = page;
    }).fail(function (data, jqXHR, ajaxOptions, thrownError) {
            alert('No response from server');
            console.log(data);
    });
}

$(document).ready(function() {
    $(document).on('click', '.pagination ul li a', function (event) {

            event.preventDefault();

            $('li').removeClass('active');

            $(this).parent('li').addClass('active');

            var myurl = $(this).attr('href');

            var page = $(this).attr('href').split('page=')[1];

            getData(page);

        }
    );
}
);