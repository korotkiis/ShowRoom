<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAutoPerformanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auto_performance', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('color');
            $table->string('engine');
            $table->string('transmission');
            $table->integer('price');
            $table->bigInteger('auto_id')->unsigned();
            $table->foreign('auto_id')->references('id')->on('auto');
//            $table->bigInteger('helper_id')->unsigned();
//            $table->foreign('helper_id')->references('id')->on('auto_helpers');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auto_performance');
    }
}
