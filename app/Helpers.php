<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Helpers extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    protected $table = 'auto_helpers';

    public function performance(){
        return $this->belongsTo('App\Performance');
    }
}
