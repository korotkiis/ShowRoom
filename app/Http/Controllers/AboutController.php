<?php
/**
 * Created by PhpStorm.
 * User: cracksasha
 * Date: 11.10.19
 * Time: 13:04
 */

namespace App\Http\Controllers;


class AboutController extends Controller
{


    public function index(){
        return view('about')->with('description', 'О нас')
            ->with('title', 'Автосалон "Центральный');
    }
}