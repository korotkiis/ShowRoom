<?php

namespace App\Http\Controllers;

use App\Auto;
use App\Helpers;
use App\Performance;
use Illuminate\Http\Request;

class CatalogController extends Controller
{
//    public function index(){
//
//
//        $autoArray = [];
//        foreach (Auto::with( 'performance', 'performance.helper')->get() as $autoKey=>$autoVal){
//            $autoArray[$autoKey] = [];
//            $autoArray[$autoKey]['label']=$autoVal->label;
//            $autoArray[$autoKey]['model']=$autoVal->model;
//            $autoArray[$autoKey]['class']=$autoVal->class;
//
//            foreach ($autoVal->performance as $perfVal){
//                $autoArray[$autoKey]['color']=$perfVal->color;
//                $autoArray[$autoKey]['engine']=$perfVal->engine;
//                $autoArray[$autoKey]['transmission']=$perfVal->transmission;
//                $autoArray[$autoKey]['price']=$perfVal->price;
//
//                $autoArray[$autoKey]['helpers'] = [];
//                foreach ($perfVal->helper as $helpKey => $helpVal){
//                    $autoArray[$autoKey]['helpers'][$helpKey]=$helpVal->name;
//                }
//            }
//
//
////            $jsonAuto = $auto->toJson(256);
//        }
//
////        echo '<pre>';
////        print_r($autoArray);
////        echo '</pre>';
//
//        return view('catalog', ['auto' => $autoArray])->with('description', 'Каталог')
//            ->with('title', 'Автосалон "Центральный');
//    }


    public function getCarPagination(Request $request){

        $auto = Auto::with( 'performance', 'performance.helper')->paginate(2);

        if ($request->ajax()){
            return view('catalog.catalog_one', ['auto' => $auto])->with('description', 'Каталог')
                ->with('title', 'Автосалон "Центральный');
        }else{
            return view('catalog.catalog_ajax', ['auto' => $auto])->with('description', 'Каталог')
                ->with('title', 'Автосалон "Центральный');
        }
    }

    public function stat(Request $request){
        if (file_exists('count.txt'))
        {
            $fil = fopen('count.txt', 'r');
            $dat = fread($fil, filesize('count.txt'));
            fclose($fil);
            $fil = fopen('count.txt', 'w');
            fwrite($fil, $dat+$_POST['status']);
        }
        else
        {
            $fil = fopen('count.txt', 'w');
            fwrite($fil, 1);
            fclose($fil);
        }


//        $fil = fopen('count.txt', 'a+');
//        fwrite($fil, $dat);
//        fclose($fil);
    }

    public function getCarAjax(Request $request){
        if ($request->ajax()){


            if (isset($_POST['start']) && is_numeric($_POST['start'])) {

                $start = $_POST['start'];
                $auto = Auto::with('performance', 'performance.helper')->offset($start)->limit(2)->get();
                echo json_encode($auto);
            }
        }
    }

    public function getCarScroll(){


        $auto = Auto::with( 'performance', 'performance.helper')->paginate(2);

        return view('catalog.catalog_scroll', ['auto'=>$auto])->with('description', 'Каталог')
            ->with('title', 'Автосалон "Центральный');
    }


}
