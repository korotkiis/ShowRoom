<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Auto extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'class', 'label', 'price', 'model'
    ];

    protected $table='auto';

    public function performance(){
        return $this->hasMany('App\Performance', 'auto_id');
    }
}
