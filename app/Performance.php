<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Performance extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'engine', 'transmission', 'color', 'price'
    ];

    protected $table = 'auto_performance';

    public function auto(){
        return $this->belongsTo('App\Auto');
    }

    public function helper(){
        return $this->hasMany('App\Helpers', 'auto_performance_id');
    }
}
